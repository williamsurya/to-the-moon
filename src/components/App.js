import React, { Component } from 'react'
import Navbar from './Navbar'
import CountDown from './CountDown'
import About from './About'
import Rocket from './Rocket'
import FlyingRocket from './FlyingRocket'
import Particles from 'react-particles-js';
import Leaderboard from './Leaderboard'
import Web3 from 'web3'

import TTMToken from '../abis/TTM.json'
import VinixToken from '../abis/VinixToken.json'

import './App.css'
import './App.scss'
import '../asset/owl.carousel.css'
import '../asset/jquery.fullPage.css'
import '../asset/animate.css'
import '../asset/magnific-popup.css'
import '../asset/style.css'
import '../asset/css'

class App extends Component {

  constructor() {
    super();
    this.handleConnect = this.handleConnect.bind(this);
    this.loadBlockchainData = this.loadBlockchainData.bind(this);
    this.checkJoined = this.checkJoined.bind(this);
    this.joinShip = this.joinShip.bind(this);
  }

  async componentDidMount() {
    await this.handleConnect();
    await this.loadBlockchainData();
  }

  state = {
    account: "",
    contractTTM: "",
    contractVinixToken: "",
    vinixTokenData: "",
    statusJoined: false,
  }

  particleParams = {
    "particles": {
      "number": {
        "value": 125,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.5,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 3,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 1,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  };

  async loadBlockchainData() {
    window.web3 = new Web3(window.web3.currentProvider)
    const web3 = window.web3

    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })

    const networkId = await web3.eth.net.getId()

    // Load TTMToken
    const ttmTokenData = TTMToken.networks[networkId];
    if (ttmTokenData) {
      console.log(ttmTokenData.address);
      const contractTTM = new web3.eth.Contract(TTMToken.abi, ttmTokenData.address);
      console.log("TTM address : " + ttmTokenData.address);
      this.setState({ contractTTM });
      await this.checkJoined();
      // this.setState({ ttmToken })
      // let ttmTokenBalance = await ttmToken.methods.balanceOf(this.state.account).call()
      // console.log(ttmTokenBalance);
      // this.setState({ daiTokenBalance: daiTokenBalance.toString() })
    } else {
      window.alert('Wrong network, please use BSC Network.');
    }

    //Load VinixToken
    const vinixTokenData = VinixToken.networks[networkId];
    if (vinixTokenData) {
      this.setState({ vinixTokenData });
      const contractVinixToken = new web3.eth.Contract(VinixToken.abi, vinixTokenData.address);
      console.log("vinix address : " + vinixTokenData.address);
      this.setState({ contractVinixToken });
      let vinixTokenBalance = await contractVinixToken.methods.balanceOf(this.state.account).call();
      console.log("balance: " + vinixTokenBalance);
    }

    this.setState({ loading: false })
  }

  async handleConnect() {
    if (window.ethereum) {
      console.log("tes");
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      console.log("tes2");
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }

  }

  async joinShip() {
    const web3 = window.web3
    const accounts = await web3.eth.getAccounts();
    this.setState({ account: accounts[0] });
    console.log(accounts[0]);
    if (accounts[0]) {
      //JOIN SHIP
      this.state.contractVinixToken.methods.approve(this.state.vinixTokenData.address, "1000000000000000000").send({ from: this.state.account }).on('transactionHash', (hash) => {
        this.state.contractTTM.methods.EnterShip(0, "1000000000000000000").send({ from: this.state.account }).on('transactionHash', (hash) => {
          console.log("BERHASIL" + hash);
        })
      })
    }
    else {
      this.handleConnect();
    }
  }

  async checkJoined() {
    const contractTTM = this.state.contractTTM;
    if (contractTTM) {
      let statusJoined = await contractTTM.methods.uniquePlayers(this.state.account).call();
      this.setState({ statusJoined });
      console.log("adress " + this.state.account);
      console.log("status join " + statusJoined);
    }
  }

  render() {
    return (
      <div className="fp-viewing-welcome image-background" style={{ position: 'relative', overflow: "hidden" }}>
        <div style={{ position: 'fixed' }}>
          <Particles height="100vh" width="100vw"
            params={this.particleParams} /></div>
        <Navbar></Navbar>
        <CountDown></CountDown>
        <About></About>
        <Rocket onJoinShip={this.joinShip} statusJoined={this.state.statusJoined}></Rocket>
        {/* <FlyingRocket></FlyingRocket> */}
        <Leaderboard></Leaderboard>
      </div >
    );
  }
}

export default App;
