import React, { Component } from 'react'
import '../css/Rocket.scss'

class FlyingRocket extends Component {
    state = {
        hours: 24,
        minutes: 0,
        seconds: 0,
        hoursString: "24",
        minutesString: "00",
        secondsString: "00"
    }

    constructor() {
        super();
        this.timer = 0;
        this.startTimer();
    }

    countDown = () => {
        let seconds = this.state.seconds;
        let minutes = this.state.minutes;
        let hours = this.state.hours;

        if (seconds <= 0) {
            if (minutes <= 0) {
                if (hours <= 0) {
                    clearInterval(this.timer);
                }
                else {
                    hours = hours - 1;
                    minutes = 59;
                    seconds = 59;
                }
            }
            else {
                minutes = minutes - 1;
                seconds = 59;
            }
        }
        else {
            seconds = seconds - 1;
        }

        let hoursString = hours;
        let minutesString = minutes;
        let secondsString = seconds;
        if (hours < 10) {
            hoursString = "0" + hours;
        }
        if (minutes < 10) {
            minutesString = "0" + minutes;
        }
        if (seconds < 10) {
            secondsString = "0" + seconds;
        }

        this.setState({ hours, minutes, seconds, hoursString, minutesString, secondsString });
    }

    startTimer = () => {
        this.timer = setInterval(this.countDown, 1000);
    }

    render() {
        return (
            <section class="section-rocket section-content section background-parallax bg-image-1 fp-section fp-table active fp-completely">
                <div class="overlay"></div>
                <div class="container">
                    <h1 class="section-title center">
                        We are <span class="highlight f-bold">launching</span> rocket to the moon
                    </h1>
                    <div class="rocket" style={{ marginTop: "0px" }}>
                        <div class="rocket-body">
                            <div class="body"></div>
                            <div class="fin fin-left"></div>
                            <div class="fin fin-right"></div>
                            <div class="window"></div>
                        </div>
                        <div class="exhaust-flame"></div>
                        <ul class="exhaust-fumes">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <ul class="star">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <div id="clock" style={{ marginTop: "250px" }}>
                        <div class="counter-container get-ready">
                            <h4>Left time to landing</h4>
                            <div class="counter-box first"><div class="number">00</div><span>Days</span></div>
                            <div class="counter-box"><div class="number">{this.state.hoursString}</div><span>Hours</span></div>
                            <div class="counter-box"><div class="number">{this.state.minutesString}</div><span>Minutes</span></div>
                            <div class="counter-box last"><div class="number">{this.state.secondsString}</div><span>Seconds</span></div>
                        </div>
                    </div>
                    <div id="prizepool">
                        <h4>With total prize pool</h4>
                        <h1>223.530 $</h1>
                    </div>
                    <div class="mg-t" style={{ textAlign: "center" }}>
                        <div class="btn btn-color btn-lg js-to-slide animated onstart fadeInUp visible" data-animation-delay="300" data-animation="fadeInUp" data-slide="4" style={{ backgroundColor: "grey", borderColor: "grey" }}>
                            Join Ship
                </div>
                        <a class="btn btn-outline btn-lg js-to-slide animated onstart fadeInUp visible" data-animation-delay="400" data-animation="fadeInUp" data-slide="2" href="#">
                            Learn more
                  <i class="fa fa-angle-down"></i>
                        </a>
                    </div>
                </div>
            </section >
        );
    }
}

export default FlyingRocket;
