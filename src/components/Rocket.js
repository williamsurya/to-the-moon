import React, { Component } from 'react'
import '../css/Rocket.scss'

class Rocket extends Component {
    state = {
        hours: 24,
        minutes: 0,
        seconds: 0,
        hoursString: "24",
        minutesString: "00",
        secondsString: "00"
    }

    constructor() {
        super();
        this.timer = 0;
        this.startTimer();
    }

    countDown = () => {
        let seconds = this.state.seconds;
        let minutes = this.state.minutes;
        let hours = this.state.hours;

        if (seconds <= 0) {
            if (minutes <= 0) {
                if (hours <= 0) {
                    clearInterval(this.timer);
                }
                else {
                    hours = hours - 1;
                    minutes = 59;
                    seconds = 59;
                }
            }
            else {
                minutes = minutes - 1;
                seconds = 59;
            }
        }
        else {
            seconds = seconds - 1;
        }

        let hoursString = hours;
        let minutesString = minutes;
        let secondsString = seconds;
        if (hours < 10) {
            hoursString = "0" + hours;
        }
        if (minutes < 10) {
            minutesString = "0" + minutes;
        }
        if (seconds < 10) {
            secondsString = "0" + seconds;
        }

        this.setState({ hours, minutes, seconds, hoursString, minutesString, secondsString });
    }

    startTimer = () => {
        this.timer = setInterval(this.countDown, 1000);
    }

    render() {
        return (
            <section className="section-rocket section-content section background-parallax bg-image-1 fp-section fp-table active fp-completely">
                <div className="overlay"></div>
                <div className="container">
                    <h1 className="section-title center">
                        We are getting <span className="highlight f-bold">ready</span> to take off
                    </h1>
                    <div className="rocket">
                        <div className="rocket-body">
                            <div className="body"></div>
                            <div className="fin fin-left"></div>
                            <div className="fin fin-right"></div>
                            <div className="window"></div>
                        </div>
                        <div className="exhaust-flame hidden"></div>
                        <ul className="exhaust-fumes hidden">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <ul className="star">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <div id="clock">
                        <div className="counter-container get-ready">
                            <h4>Left time to take off</h4>
                            <div className="counter-box first"><div className="number">00</div><span>Days</span></div>
                            <div className="counter-box"><div className="number">{this.state.hoursString}</div><span>Hours</span></div>
                            <div className="counter-box"><div className="number">{this.state.minutesString}</div><span>Minutes</span></div>
                            <div className="counter-box last"><div className="number">{this.state.secondsString}</div><span>Seconds</span></div>
                        </div>
                    </div>
                    <div id="prizepool">
                        <h4>With total prize pool</h4>
                        <h1>223.530 $</h1>
                    </div>
                    {
                        !this.props.statusJoined ?
                            <div className="mg-t" style={{ textAlign: "center" }}>
                                <div onClick={this.props.onJoinShip} className="btn btn-color btn-lg js-to-slide animated onstart fadeInUp visible" data-animation-delay="300" data-animation="fadeInUp" data-slide="4">
                                    Join Ship
                                </div>
                                <a className="btn btn-outline btn-lg js-to-slide animated onstart fadeInUp visible" data-animation-delay="400" data-animation="fadeInUp" data-slide="2" href="#">
                                    Learn more
                                <i className="fa fa-angle-down"></i>
                                </a>
                            </div> 
                            :
                            <div className="mg-t" style={{ textAlign: "center" }}>
                            <h5>You have been joined ship, get ready to take off.</h5>
                            <a className="btn btn-outline btn-lg js-to-slide animated onstart fadeInUp visible" data-animation-delay="400" data-animation="fadeInUp" data-slide="2" href="#">
                                Learn more
                            <i className="fa fa-angle-down"></i>
                            </a>
                        </div> 
                    }
                </div>
            </section >
        );
    }
}

export default Rocket;
