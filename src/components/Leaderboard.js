import React, { Component } from 'react'
import '../css/Leaderboard.css'

class Leaderboard extends Component {


    render() {
        return (
            <section className="about section background-parallax bg-image-2 fp-section fp-table" data-anchor="about" style={{ height: 600 }}><div className="fp-tableCell" style={{ height: 600 }}>
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="hero-content">
                                <h1 className="section-title text-left animated fadeInUp visible" data-animation-delay="0" data-animation="fadeInUp">
                                    <span className="highlight f-bold">Moon</span> citizen's
                                </h1>
                            </div>
                            <div className="row">
                                <div className="leaderboard">
                                    <div className="header">
                                        <div className="header-title address">Address</div>
                                        <div className="header-title prize">Prize Pool</div>
                                        <div className="header-title time">Date</div>
                                    </div>
                                    <div className="rank">
                                        <div className="address">0x8A9a4Bf7fC8C6b711b7350022D8595919E05D22e</div>
                                        <div className="prize">20.515 $</div>
                                        <div className="time">22 September 2020</div>
                                    </div>
                                    <div className="rank">
                                        <div className="address">0x6b711A9a4Bf7b795919E05D28fC8C2350022D85e</div>
                                        <div className="prize">13.420 $</div>
                                        <div className="time">21 September 2020</div>
                                    </div>
                                    <div className="rank">
                                        <div className="address">0x711b73502D88C8C6b595A9aE0025D4Bf7f91922e</div>
                                        <div className="prize">5.220 $</div>
                                        <div className="time">20 September 2020</div>
                                    </div>
                                    <div className="rank">
                                        <div className="address">0x9E05D22fC8C6b711b7350022D88A9a4Bf759591e</div>
                                        <div className="prize">4.535 $</div>
                                        <div className="time">19 September 2020</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></section>
        );
    }
}

export default Leaderboard;
