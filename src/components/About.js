import React, { Component } from 'react'

class About extends Component {


    render() {
        return (
            <section className="about section background-parallax bg-image-2 fp-section fp-table" data-anchor="about" style={{ height: 600 }}><div className="fp-tableCell" style={{ height: 600 }}>
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="hero-content">
                                <h1 className="section-title text-left animated fadeInUp visible" data-animation-delay="0" data-animation="fadeInUp">
                                    To the moon with us
                </h1>
                            </div>
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="mg-b">
                                        <p className="animated fadeInUp visible" data-animation-delay="100" data-animation="fadeInUp">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A voluptatibus minus quasi facilis, aliquid ratione molestias,
                                            autem natus molestiae, accusamus sit recusandae iure voluptas
                                            atque ipsam assumenda tenetur rerum repellendus?
                    </p>
                                    </div>
                                    <a className="btn btn-outline btn-lg js-link-video animated fadeInUp visible" data-animation-delay="200" data-animation="fadeInUp" data-slide="4" href="http://www.youtube.com/watch?v=Zs8T-FCEvXg">
                                        Watch Video
                    <i className="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></section>
        );
    }
}

export default About;
