import React, { Component } from 'react'

class CountDown extends Component {
    state = {
        hours: 24,
        minutes: 0,
        seconds: 0,
        hoursString: "24",
        minutesString: "00",
        secondsString: "00"
    }

    constructor() {
        super();
        this.timer = 0;
        this.startTimer();
    }

    countDown = () => {
        let seconds = this.state.seconds;
        let minutes = this.state.minutes;
        let hours = this.state.hours;

        if (seconds <= 0) {
            if (minutes <= 0) {
                if (hours <= 0) {
                    clearInterval(this.timer);
                }
                else {
                    hours = hours - 1;
                    minutes = 59;
                    seconds = 59;
                }
            }
            else {
                minutes = minutes - 1;
                seconds = 59;
            }
        }
        else {
            seconds = seconds - 1;
        }

        let hoursString = hours;
        let minutesString = minutes;
        let secondsString = seconds;
        if (hours < 10) {
            hoursString = "0" + hours;
        }
        if (minutes < 10) {
            minutesString = "0" + minutes;
        }
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } 

        this.setState({ hours, minutes, seconds, hoursString, minutesString, secondsString });
    }

    startTimer = () => {
        this.timer = setInterval(this.countDown, 1000);
    }

    render() {
        return (
            <section className="welcome section-content section background-parallax bg-image-1 fp-section fp-table active fp-completely" data-anchor="welcome" style={{ height: 600 }}><div className="fp-tableCell" style={{ height: 600 }}>
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1 className="section-title">
                                We are going <span className="highlight f-bold">take off</span> soon
              </h1>
                            <div id="clock"><div className="counter-container"><div className="counter-box first"><div className="number">00</div><span>Days</span></div>
                                <div className="counter-box"><div className="number">{this.state.hoursString}</div><span>Hours</span></div>
                                <div className="counter-box"><div className="number">{this.state.minutesString}</div><span>Minutes</span></div>
                                <div className="counter-box last"><div className="number">{this.state.secondsString}</div><span>Seconds</span></div></div></div>
                            <div className="mg-t">
                                <a className="btn btn-color btn-lg js-to-slide animated onstart fadeInUp visible" data-animation-delay="300" data-animation="fadeInUp" data-slide="4" href="#">
                                    Notify Me
                </a>
                                <a className="btn btn-outline btn-lg js-to-slide animated onstart fadeInUp visible" data-animation-delay="400" data-animation="fadeInUp" data-slide="2" href="#">
                                    Learn more
                  <i className="fa fa-angle-down"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        );
    }
}

export default CountDown;
