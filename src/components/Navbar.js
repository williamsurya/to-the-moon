import React, { Component } from 'react'

class Navbar extends Component {

  render() {
    return (

      <header className="page-header" id="page-header">
        <div className="container-full">
          <div>
            <div className="navbar-header">
              <a className="logo animated onstart fadeInDown visible" data-animation-delay="200" data-animation="fadeInDown" href="#">
                <div className="circle-logo-wrapper">
                  <div className="circle"></div>
                  <i className="icon fa fa-rocket" aria-hidden="true"></i>
                </div>
                <strong> To</strong> The Moon
              </a>
            </div>

            <div className="social-nav">
              <ul>
                <li className="animated onstart fadeInDown visible" data-animation-delay="600" data-animation="fadeInDown" href="#">
                  <a data-placement="bottom" data-toggle="tooltip" href="#" title="Follow us on Facebook">
                    <i className="fa fa-facebook"></i>
                  </a>
                </li>
                <li className="animated onstart fadeInDown visible" data-animation-delay="700" data-animation="fadeInDown" href="#">
                  <a data-placement="bottom" data-toggle="tooltip" href="#" title="Follow us on Twitter">
                    <i className="fa fa-twitter"></i>
                  </a>
                </li>
                <li className="animated onstart fadeInDown visible" data-animation-delay="800" data-animation="fadeInDown" href="#">
                  <a data-placement="bottom" data-toggle="tooltip" href="#" title="Follow us on Instagram">
                    <i className="fa fa-instagram"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Navbar;
